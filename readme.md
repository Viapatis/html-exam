# Вёрстка

Необходимо сверстать следующий [макет](https://www.figma.com/file/09ZjeMaUZqEV6AAqk3BEKPcE/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2?node-id=0%3A1)

## Требования к вёрстке

* проходит [валидацию](https://validator.w3.org/#validate_by_upload)

### Шаблон

* оформлена структура (шапка, футер, контент);
* футер прибит книзу;
* задана минимальная (900) и максимальная (1440) ширина для контента;
* в шапке сверстаны логотип и корзина, разнесены по краям;
* копирайт в футере;
* для заданной в макете ширины контента (1200) всё примерно соответствует пропорциям макета.

### Контент

* серый фон меню занимает всю доступную высоту (вертикального скролла нет, если содержимое не заполняет всю страницу);
* у ссылок в меню есть эффект при наведении;
* у хлебных крошек стрелки сделаны псевдо-элементами;
* у ссылок хлебных крошек есть эффект при наведении;
* есть разметка товара в каталоге (фотка, ссылка с заголовком, артикул, цена и кнопка);
* ссылка товара имеет эффект при наведении;
* кнопка содержит иконку корзины;
* цена и кнопка прибиты книзу карточки;
* все блоки с фотографией товара имеют одинаковый размер;
* все фотографии сужаются на маленьких экранах;
* фотографии выровнены по высоте по центру своей области.
